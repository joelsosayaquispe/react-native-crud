import IProduct from '../interfaces/product.interface';

export const products:IProduct[] = [
  {
    id: "1",
    name: 'Licuadora',
    description: 'Marca oster',
    price: 150
  },
  {
    id: "2",
    name: 'Televisor',
    description: 'Marca Lg',
    price: 1100
  },
  {
    id: "3",
    name: 'Iphone',
    description: '256 gb de almacenamiento',
    price: 2000
  },
  {
    id: "4",
    name: 'Smartphone xiaomi redmi 8 pro',
    description: '6 gb ram, 64 gb de almacenamiento',
    price: 1000
  },
  
];

